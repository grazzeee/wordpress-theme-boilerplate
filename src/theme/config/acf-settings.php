<?php

	//Add ACF options page to theme
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page();
	}

	// Remove empty p tags from editor
	function remove_empty_p( $content ) {
		$content = force_balance_tags( $content );
		$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
		$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
		return $content;
	}
	add_filter('the_content', 'remove_empty_p', 20, 1);
	remove_filter ('acf_the_content', 'wpautop');

	//Remove WPAUTOP from ACF TinyMCE Editor
	function acf_wysiwyg_remove_wpautop() {
	    remove_filter('acf_the_content', 'wpautop' );
	}
	add_action('acf/init', 'acf_wysiwyg_remove_wpautop');

?>
