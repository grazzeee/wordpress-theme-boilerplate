$(document).ready(function(){

	// bind filter button click
	$('.mobile-menu-wrap').on( 'click', 'button', function() {
		$('.hamburger').toggleClass("is-active");
		$('header').toggleClass("mobile-menu-is-active");
	});

	// FORCE MOBILE MENU TO SHUT WHEN SCOLLED TO LANDSCAPE
	$(window).resize(function(){
		var w=window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		var h=window.innerHeight || document.documentElement.clientHeight ||document.body.clientHeight;

		if (w >= 1024) {
			$('.hamburger').removeClass("is-active");
			$('header').removeClass("mobile-menu-is-active");
		}
	});

	// STICKY NAV
	var userScroll = $(document).scrollTop();
	$(window).on('scroll', function() {
	   	var newScroll = $(document).scrollTop();
	   	if (newScroll > 40 || userScroll > 40) {
		  $('body').addClass('user-has-scrolled');
	   	} else {
			$('body').removeClass('user-has-scrolled');
	   	}
	});

});