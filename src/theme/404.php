<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="flexible-content-row row-latest_job_posts row_index-2 marginDefault paddingSmall backgroundWhite locationNormal">

			<!-- article -->
			<article class="contentText postNotFound" id="post-404">

			<div id="notfound">
				<div class="notfound">
					<div class="notfound-404"></div>
					<h1>404</h1>
					<h2>Oops! Page Not Be Found</h2>
					<p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
					<a href="#">Back to homepage</a>
				</div>
			</div>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->

	</main>

<?php get_footer(); ?>
