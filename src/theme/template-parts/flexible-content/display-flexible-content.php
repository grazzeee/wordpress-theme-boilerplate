<?php
    $flexIndex = 1;

    if (have_rows('flexible_content_rows')) :
        while (have_rows('flexible_content_rows')) : the_row();

            echo '<section class="flexible-content-row row-'.get_row_layout().' row_index-'.$flexIndex.'">';

                if ( $anchor_tag != '' ) {
                    echo '<div class="anchorLink" id="'.$anchor_tag.'"></div>';
                }

                $file = get_stylesheet_directory() . "/template-parts/flexible-content-rows/" . get_row_layout() . ".php";
                include($file);

            echo '</section>';

            $flexIndex++;

        endwhile;
    endif;

    $flex_name = false;
?>
