<div class="background-hero-images">
    <div class="img-mobile">
		<div class="fill-container background-image animated slow fadeIn exspand-over-time" style="background-image: url(<?php echo aq_resize( $background_image['url'], 1280, 1920, true, true, true ) ?>)"></div>
    </div>
    <div class="img-tablet-port">
        <div class="fill-container background-image animated slow fadeIn exspand-over-time" style="background-image: url(<?php echo aq_resize( $background_image['url'], 1536, 2048, true, true, true ) ?>)"></div>
    </div>
    <div class="img-tablet-land">
        <div class="fill-container background-image animated slow fadeIn exspand-over-time" style="background-image: url(<?php echo aq_resize( $background_image['url'], 2048, 1536, true, true, true ) ?>"></div>
    </div>
    <div class="img-desktop">
        <div class="fill-container background-image animated slow fadeIn exspand-over-time" style="background-image: url(<?php echo aq_resize( $background_image['url'], 2560, 1440, true, true, true ) ?>"></div>
    </div>
</div>

