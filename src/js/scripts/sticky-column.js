$(document).ready(function(){

	// Stick content block to column
	var userScroll = $(document).scrollTop();
	$(window).on('scroll', function() {
		if ($(".sticky-column-content")[0]){
			var newScroll = $(document).scrollTop();
			var fixedWidth = $('.column-fixed').width();

			if (newScroll > 40 || userScroll > 40) {
		   		$('.sticky-column-content').addClass('position-fixed');
				$('.sticky-column-content').css('width', fixedWidth);
			} else {
				$('.sticky-column-content').removeClass('position-fixed');
				$('.sticky-column-content').css('width', 'auto');
			}
		}
	});

	if ($("#sticky-content-trigger")[0]){
		// Check footer is in view
		var footerInView = new Waypoint({
			element: $('#sticky-content-trigger'),
			handler: function(direction) {
				if (direction == 'up') {
					if ($(".sticky-column-content")[0]){
						$('.sticky-column-content').removeClass('align-bottom');
					}
				} else {
					if ($(".sticky-column-content")[0]){
						$('.sticky-column-content').addClass('align-bottom');
					}
				}
			},
			offset: '90%'
		});
	}
});