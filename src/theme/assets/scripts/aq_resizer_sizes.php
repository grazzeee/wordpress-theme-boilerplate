<?php


/*------------------------------------*\
    HERO IMAGE FULL SCREEN
\*------------------------------------*/
    $HomeImageFullScreenRatioMobile = '640x960';
    $HomeImageFullScreenRatioTabletPort = '1536x2048';
    $HomeImageFullScreenRatioTabletLand = '2048x1536';
    $HomeImageFullScreenRatioDesktop = '3840x2160';

    $HomePageHeroImageSizesArrayMobile = array(
        array('w' => 320, 'upsize' => true),
        array('w' => 440, 'upsize' => true),
        array('w' => 540, 'upsize' => true),
        array('w' => 640, 'upsize' => true)
    );

    $HomePageHeroImageSizesArrayTabletPort = array(
        array('w' => 800, 'upsize' => true),
        array('w' => 980, 'upsize' => true),
        array('w' => 1200, 'upsize' => true),
        array('w' => 1350, 'upsize' => true)
    );

    $HomePageHeroImageSizesArrayTabletLand = array(
        array('w' => 1400, 'upsize' => true),
        array('w' => 1600, 'upsize' => true),
        array('w' => 1750, 'upsize' => true),
        array('w' => 1900, 'upsize' => true)
    );

    $HomePageHeroImageSizesArrayDesktop = array(
        array('w' => 1900, 'upsize' => true),
        array('w' => 2100, 'upsize' => true),
        array('w' => 2300, 'upsize' => true),
        array('w' => 2500, 'upsize' => true),
        array('w' => 2700, 'upsize' => true),
        array('w' => 2900, 'upsize' => true),
        array('w' => 3100, 'upsize' => true),
        array('w' => 3300, 'upsize' => true),
        array('w' => 3500, 'upsize' => true),
        array('w' => 3700, 'upsize' => true)
    );
?>
