<?php
define('WP_CACHE', true); // Added by Cache Enabler
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'graeme-test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M5$)e)smTS:@jg!Zoe%%WOL`7;=WDYc,LTK3>O;f,TmgSYXOdV[3^g&][U&k%[*P');
define('SECURE_AUTH_KEY',  'o<#RMGng}@u;yD@Ek2((No?9IB#k4zU4c14jWC(O-5zkS1BDL>wFV ,[MnuGEx2O');
define('LOGGED_IN_KEY',    'qN$?nifu*/I5r>Q8)tQ`0q)CX8>TA~:T%YtSB%aCD!r1W?Y U.?f4QQg9hOLp>%6');
define('NONCE_KEY',        '}&8.tQ*s6@*js=% J=X@AjXe ]`1=Ogi~SaJxJ$TZho%)h*m*@EMwvCw.7t0]`Q:');
define('AUTH_SALT',        'If@H3tHu:R$r2mE<ydF J#ZqJh~Ckg0Rk1,HSd}7oxoV*+4%SE%@cKE<@1Cw).|6');
define('SECURE_AUTH_SALT', 'jIGz lhp]k3CgVTXiY:rjjhScrg!7Sn#SvGX:Yf-:rx<K95,P0#2BKw?X!gV~LRY');
define('LOGGED_IN_SALT',   '@g=J[pja&~~aI+]:TET!hw-M.bjzu#XGaWhH4HQ~4E-<lV/.VkL,gTJGo;NM:)}b');
define('NONCE_SALT',       '[q?Y{zK0G5:p=._`|BESMR1y:IQJ&<!<zX@Iou|bIkh4,*[JSwI9cgL:{lex82mA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
