Automate Wordpress
====================
Automate wordpress is a simple yet functional automated build process to help you construct the foundations of your new theme in minutes.


- - -

What you get
================================

1. Pre configured package.json including the following packages
2. Pre configured bower.json including the following wordpress plugins and wordpress install
3. Pre configured gulp.js with settings to allow for a custom build
4. Basic theme files to help get any Wordpress theme off the ground


- - -

Folder structure
================================

This is a quick breakdown of what the folder structure looks like

			- /dist
			- /src
				- /css
				- /js
					- /scripts
				- /sass
				- /svg
				- /theme
			- /wordpress-setup


/dist - This folder gets created when the theme is built using '$ gulp theme'. 
	1. Wordpress get installed from the bower_components 
	2. Theme plugins get installed form bower_components
	4. Theme files get copied to the wordpress theme folder

/src/css - Any css files that are needed get placed here. They get copied over when '$ gulp theme' is executed. These files then get minified and merged into one file that sits within the theme assets folder located here '/dist/wp-content/themes/theme-name/assets/css/plugin-styles.min.css' when the '$ gulp watch' is run.

/src/js/scripts - Place any theme jquery/javascript files here. They get minified and merged into one file that sits within the theme assets folder located here '/dist/wp-content/themes/theme-name/assets/js/scripts.min.js'

/src/sass - All the sass files are compiled on the '$ gulp watch' and moved into the themes css folder located here '/dist/wp-content/themes/theme-name/assets/css/style.min.css'

/src/svg - All the SVG files are compressed on the '$ gulp watch' and moved into the themes /svg folder located here '/dist/wp-content/themes/theme-name/assets/svg'

src/theme - All files saved within this theme folder get copied to the main theme location here '/dist/wp-content/themes/theme-name/'

/wordpress-setup - Any custom files needed to run wordpress are saved here, for example a custom .htaccess file or wp-config file. These get copied over on '$ gulp theme'


- - -

list of extensions used
================================

1. List of librarys used  
		1. conditionizr  
		2. modernizr  
		3. animate.css  
		4. counterup  
		5. isotope-layout  
		6. flickity  
		7. featherlight  
		8. hamburgers  
		9. matchheight  
		10. font-awesome  
		11. reset-css  
		12. select2  
		13. slick-carousel  
		14. jquery.waitforimages  
		15. waypoints  
		16. Gulp packages that help automate our build process  

1. List of wordpress plugins that are saved in bower.   
		1. advanced-custom-fields  
		2. acf-flexible-layouts-manager  
		3. acf-audio-video-player  
		4. advanced-custom-fields-font-awesome  
		5. acf-vimeo-field  
		6. better-search-replace  
		7. cache-enabler  
		8. custom-post-type-ui  
		9. disable-comments  
		10. disable-gutenberg  
		11. duplicate-post  
		12. enhanced-media-library  
		13. ninja-forms  
		14. post-type-switcher  
		15. post-types-order  
		16. wp-maintenance-mode  
		17. wordpress-seo  


- - -

Optional build options
================================
you can remove any libraries from the build process by setting the option to false in the gulp file.


- - -

Custom theme name
================================
You can change the theme folder name by modifying the theme name variable in the gulp file. 

	var themeName = 'wordpress-theme';


- - -

Gulp Commands 
================================

"Gulp theme is used to start the build process. When this is run the following happens"

	$ gulp theme

1. Any libraries that are turned on in the gulp file are copied to the correct locations within the /src folder.
2. Wordpress core gets coppied to /dist 
3. Any custom files needed to make wordpress work gets coppied over to the /dist folder 
4. Wordpress plugins get copied from the bower_components folder
5. Any files located within the /src/theme folder get copied to the wordpress theme folder located in the /dist/wp-content/themes/theme-name

"Gulp watch is used when editing the theme src folder. When any changes are saved to the /theme, /sass or /js folders within the /src the theme is rebuilt so the changes can be viewed in the browser."

	$ gulp watch

1. All CSS & SASS get minified and mereged into two files within the wordpress theme folder located in  /dist/wp-content/themes/theme-name/assets/css
2. All js/javascript files are uglified and merged into two files within the wordpress theme folder located in /dist/wp-content/themes/theme-name/assets/js
3. All the SVG files are compressed and moved into the themes /svg folder located here '/dist/wp-content/themes/theme-name/assets/svg'
4. Any files located within the /src/theme folder get copied to the wordpress theme folder located in the /dist/wp-content/themes/theme-name


"Gulp clean is used to remove all the files within the /dist folder. Anything saved within /wp-content will not be delted. This is to prevent any uploaded images and files to wordpress being removed"

	$ gulp clean

- - -

Setup Instructions
================================

Install npm packages

	$ npm install


Install bower packages

	$ npm run bower install


Build theme

	$ gulp theme

Start watching for changes and compile latest js/css assets

	$ gulp watch