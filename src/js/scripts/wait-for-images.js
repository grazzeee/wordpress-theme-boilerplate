$(document).ready(function(){

	if ($(".fci-hero")[0]){
		heroImageLoadIn();
	}

	function loadAllHeroImages() {
		$('.img-mobile').children('.background-image').addClass('loaded');
		$('.img-tablet-port').children('.background-image').addClass('loaded');
		$('.img-tablet-land').children('.background-image').addClass('loaded');
		$('.img-desktop').children('.background-image').addClass('loaded');
	}

	function heroImageLoadIn() {
		var w=window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (w <= 768) {
			$('.img-mobile').children('.background-image').waitForImages(function() {
				$(this).addClass('loaded animated fadeIn slow');
			});

			setTimeout(function(){
				loadAllHeroImages();
			}, 3000);
		} else if (w <= 1024) {
			$('.img-tablet-port').children('.background-image').waitForImages(function() {
				$(this).addClass('loaded animated fadeIn slow');
			});

			setTimeout(function(){
				loadAllHeroImages();
			}, 3000);
		} else if (w <= 1280) {
			$('.img-tablet-land').children('.background-image').waitForImages(function() {
				$(this).addClass('loaded animated fadeIn slow');
			});

			setTimeout(function(){
				loadAllHeroImages();
			}, 3000);
		} else {
			$('.img-desktop').children('.background-image').waitForImages(function() {
				$(this).addClass('loaded animated fadeIn slow');
			});

			setTimeout(function(){
				loadAllHeroImages();
			}, 3000);
		}
	}
});