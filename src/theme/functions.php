<?php

// - - - - - - - - - - - - /
// ENQUEUE SETTINGS
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/config/enqueue.php");


// - - - - - - - - - - - - /
// ADVANCED CUSTOM FIELD SETTINGS
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/config/acf-settings.php");


// - - - - - - - - - - - - /
// NAVIGATION SETTINGS
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/config/navigation.php");


// - - - - - - - - - - - - /
// CUSTOM THEME SUPPORT
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/config/theme-support.php");


// - - - - - - - - - - - - /
// CUSTOM POST TYPE SUPPORT
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/config/post-types.php");


// - - - - - - - - - - - - /
// LOAD THIRD PARTY SCRIPTS
// - - - - - - - - - - - - /
    require_once(get_stylesheet_directory() . "/assets/scripts/aq_resizer.php");
 	require_once(get_stylesheet_directory() . "/assets/scripts/aq_resizer_srcset.php");
    require_once(get_stylesheet_directory() . "/assets/scripts/aq_resizer_sizes.php");

?>
