<?php

	//remove taxonomy from custom post type right column
	register_taxonomy( 'tax_portfolio_categories', array( 'cpt_our_work' ), $args );
	$args = array(
		'show_ui'                    => true,
		'show_in_quick_edit'         => false,
		'meta_box_cb'                => false,
	);

    // Remove Admin bar
    function remove_admin_bar(){
        return false;
    }
	add_filter('show_admin_bar', 'remove_admin_bar');

	// Flush theme rewrite rules
	add_action( 'after_switch_theme', 'flush_rewrite_rules' );
	register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
	register_activation_hook( __FILE__, 'myplugin_flush_rewrites' );
	function myplugin_flush_rewrites() {
		myplugin_custom_post_types_registration();
		flush_rewrite_rules();
	}

	// Add SVG to allowed file uploads
	function add_file_types_to_uploads($file_types){

		$new_filetypes = array();
		$new_filetypes['svg'] = 'image/svg+xml';
		$file_types = array_merge($file_types, $new_filetypes );

		return $file_types;
	}
	add_action('upload_mimes', 'add_file_types_to_uploads');

	// Remove default text input fields from pages
	add_action('admin_init', 'remove_textarea');
    function remove_textarea() {
        remove_post_type_support( 'page', 'editor' );
	}

	// Add slug to page body class
	function add_slug_body_class( $classes ) {
		global $post;

		$page_header_style = get_field('page_header_style');
		$menu_fixed = get_field('menu_fixed_page');

		if (isset($post)) {
		    $classes[] = $post->post_type .'-'.$post->post_name.' '.$page_header_style.' '.$page_background_style.' '.$menu_fixed;
		}

		return $classes;
	}
	add_filter('body_class', 'add_slug_body_class');

	// Add browser and osx to body class
	function mv_browser_body_class($classes) {
	        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
	        if($is_lynx) $classes[] = 'lynx';
	        elseif($is_gecko) $classes[] = 'gecko';
	        elseif($is_opera) $classes[] = 'opera';
	        elseif($is_NS4) $classes[] = 'ns4';
	        elseif($is_safari) $classes[] = 'safari';
	        elseif($is_chrome) $classes[] = 'chrome';
	        elseif($is_IE) {
	                $classes[] = 'ie';
	                if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
	                $classes[] = 'ie'.$browser_version[1];
	        } else $classes[] = 'unknown';
	        if($is_iphone) $classes[] = 'iphone';
	        if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
	                 $classes[] = 'osx';
	           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
	                 $classes[] = 'linux';
	           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
	                 $classes[] = 'windows';
	           }
	        return $classes;
	}
	add_filter('body_class','mv_browser_body_class');

	// Remove default post type from side menu
	add_action( 'admin_menu', 'remove_default_post_type' );
	function remove_default_post_type() {
	    remove_menu_page( 'edit.php' );
	}

	#The +New Post in Admin Bar
	add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );
	function remove_default_post_type_menu_bar( $wp_admin_bar ) {
	    $wp_admin_bar->remove_node( 'new-post' );
	}

	// remove user profile avatar
	update_option( 'show_avatars', 0 );
	add_filter( 'option_show_avatars', '__return_false' );
	add_action( 'load-profile.php', function()
	{
	   add_filter( 'option_show_avatars', '__return_false' );
	} );

	// remove user bio and info fields
	add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );

	class T5_Hide_Profile_Bio_Box {

		public static function start() {
			$action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
			add_action( $action, array ( __CLASS__, 'stop' ) );
			ob_start();
		}

		public static function stop() {
			$html = ob_get_contents();
			ob_end_clean();

			// remove the headline
			$headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
			$html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

			// remove the table row
			$html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
			print $html;
		}
	}
?>