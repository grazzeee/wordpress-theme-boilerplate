$(document).ready(function(){


	// SLICK CAROUSEL
	var transition = $('.carouselSlider').data('transition');
	if ( transition == 'transSlide') {
		var fadeOption = false;
	} else {
		var fadeOption = true;
	}

	var autoPlaySpeed = $('.carouselSlider').data('auto-play');
	if ( autoPlaySpeed == 'playOff') {
		var autoPlay = false;
		var PlaySpeed = 0;
	}else if ( autoPlaySpeed == 'playSlow') {
		var autoPlay = true;
		var PlaySpeed = 5000;
	}else if ( autoPlaySpeed == 'playMedium') {
		var autoPlay = true;
		var PlaySpeed = 3000;
	}else if ( autoPlaySpeed == 'playFast') {
		var autoPlay = true;
		var PlaySpeed = 1500;
	}

	var opts = {
		fade: fadeOption,
		autoplay: autoPlay,
		autoplaySpeed: PlaySpeed,
		infinite: true,
		speed: 1000,
		adaptiveHeight: false,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
	};

	var members = {
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		nextArrow: '<div class="carouselArrow carouselSliderNext"><span class="icon-chevron-thin-right"></span></div>',
		prevArrow: '<div class="carouselArrow carouselSliderPrev"><span class="icon-chevron-thin-left"></span></div>',
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
					slidesToShow: 4,
			  }
			},
			{
			  breakpoint: 768,
				settings: {
					slidesToShow: 3,
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
					slidesToShow: 2,
			  }
			}
		]
	};

	var logo = {
		autoplay: true,
		autoplaySpeed: 1000,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		variableWidth: true,
		adaptiveHeight: false,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
	};

	var latestNews = {
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		dots: true,
		responsive: [
			{
			  breakpoint: 900,
			  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
			  }
			}
		]
	};

	$('.carouselSlider').slick(opts);
	$('.team-member-carousel').slick(members);
	$('.latest-news-carousel').slick(latestNews);
	$('.logo-carousel').slick(logo);

});