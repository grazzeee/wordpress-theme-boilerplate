/**
 * Theme Settings
 * This will be the name of the wordpress theme folder
 */

var themeName = 'wordpress-theme';

/**
 * Build Settings
 * Turn on/off build settings.
 * These take place on the watch and build 
 */

var settings = {
	clean: true, // Clean 
	scripts: true, // Compile and minify JS files into theme
	polyfills: true, // Create JS Pollyfills and add to theme
	styles: true, // Compile SASS files into into theme
	pluginStyles: true, // Compile js library plugin styles
	svgs: true, // Compress and move svg files into theme
	wordpressCore: true, // Copy wordpress core files
	wordpressSetup: true, // Copy wordpress setup files
	wordpressPlugins: true, // Copy wordpress plugins from bower 
	copy: true,
	reload: false
};

/**
 * Theme scripts
 * Turn on/off in the build
 */

var libraries = {
	installJs: 'src/js/lib/',
	installCss: 'src/css/',
	resetCss: {
		enabled: true,
		css: 'node_modules/reset-css/reset.css',
	},
	conditionizr: {
		enabled: true,
		js: 'node_modules/conditionizr/dist/conditionizr.js',
	},
	animateCss: {
		enabled: true,
		css: 'node_modules/animate.css/animate.css',
	},
	counterup: {
		enabled: true,
		js: 'node_modules/counterup/jquery.counterup.min.js',
	},
	slick_carousel: {
		enabled: true,
		js: 'node_modules/slick-carousel/slick/slick.js',
		css: ['node_modules/slick-carousel/slick/slick.css', 
					'node_modules/slick-carousel/slick/slick-theme.css']
	},
	isotope_layout: {
		enabled: true,
		js: 'node_modules/isotope-layout/dist/isotope.pkgd.js',
	},
	flickity:{
		enabled: true,
		js: 'node_modules/flickity/dist/flickity.pkgd.min.js',
		css: 'node_modules/flickity/dist/flickity.css'

	},
	featherlight: {
		enabled: true,
		js: 'node_modules/featherlight/release/featherlight.min.js',
	},
	fontAwesome: {
		enabled: true,
		css: 'node_modules/font-awesome/css/font-awesome.css',
	},
	hamburgers: {
		enabled: true,
		css: 'node_modules/hamburgers/dist/hamburgers.css',
	},
	matchheight: {
		enabled: true,
		js: 'node_modules/matchheight/dist/MatchHeight.min.js',
	},
	select2: {
		enabled: true,
		js: 'node_modules/select2/dist/js/select2.full.min.js',
		css: 'node_modules/select2/dist/css/select2.min.css',
	},
	waitforimages: {
		enabled: true,
		js: 'node_modules/jquery.waitforimages/dist/jquery.waitforimages.js',
	},
	waypoints: {
		enabled: true,
		js: 'node_modules/waypoints/lib/jquery.waypoints.js',
	},
};


/**
 * Paths to project folders
 * Set project paths
 */

var paths = {
	input: 'src/',
	output: 'dist/',
	clean: ['dist/',
					'!wp-content/uploads/**'],
	themelocation: 'dist/wp-content/themes/',
	scripts: {
		input: 'src/js/*',
		polyfills: '.polyfill.js',
		output: 'dist/wp-content/themes/'+ themeName +'/assets/js/'
	},
	styles: {
		input: 'src/sass/style.scss',
		output: 'dist/wp-content/themes/'+ themeName +'/assets/css/'
	},
	pluginStyles: {
		input: 'src/css/**/*',
		output: 'dist/wp-content/themes/'+ themeName +'/assets/css/'
	},
	svgs: {
		input: 'src/svg/*.svg',
		output: 'dist/wp-content/themes/'+ themeName +'/assets/svg/'
	},
	wordpressCore: {
		input: 'bower_components/wordpress/**/*',
		output: 'dist/'
	},
	wordpressSetup: {
		input: 'wordpress-setup/**/*',
		output: 'dist/'
	},
	wordpressPlugins: {
		input: ['bower_components/**/*',
						'!bower_components/wordpress/**'],
		output: 'dist/wp-content/plugins/'
	},
	copy: {
		input: 'src/theme/**/*',
		output: 'dist/wp-content/themes/'+ themeName +'/'
	},
	reload: './dist/'
};


/**
 * Template for banner to add to file headers
 */

var banner = {
	full:
		'/*!\n' +
		' * <%= package.name %> v<%= package.version %>\n' +
		' * <%= package.description %>\n' +
		' * (c) ' + new Date().getFullYear() + ' <%= package.author.name %>\n' +
		' * <%= package.license %> License\n' +
		' * <%= package.repository.url %>\n' +
		' */\n\n',
	min:
		'/*!' +
		' <%= package.name %> v<%= package.version %>' +
		' | (c) ' + new Date().getFullYear() + ' <%= package.author.name %>' +
		' | <%= package.license %> License' +
		' | <%= package.repository.url %>' +
		' */\n'
};


/**
 * Gulp Packages
 */

// General
var {gulp, src, dest, watch, series, parallel} = require('gulp');
var del = require('del');
var flatmap = require('gulp-flatmap');
var lazypipe = require('lazypipe');
var rename = require('gulp-rename');
var header = require('gulp-header');
var package = require('./package.json');
var plumber = require('gulp-plumber');

// Scripts
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var optimizejs = require('gulp-optimize-js');

// Styles
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var minify = require('gulp-cssnano');
var cleanCSS = require('gulp-clean-css');

// SVGs
var svgmin = require('gulp-svgmin');

// BrowserSync
var browserSync = require('browser-sync');

/**
 * Gulp Tasks
 */

// Remove pre-existing content from output folders
var cleanTheme = function (done) {

	// Make sure this feature is activated before running
	if (!settings.clean) return done();

	// Clean the dist folder
	del.sync([
		paths.themelocation + themeName
	]);

	// Signal completion
	return done();

};

var cleanDist = function (done) {

	// Make sure this feature is activated before running
	if (!settings.clean) return done();

		return del([
			'dist/**/*',
			'!dist/wp-content/**'
		]);
		
	// Signal completion
	return done();

};

// Repeated JavaScript tasks
var jsTasks = lazypipe()
	.pipe(header, banner.full, {package: package})
	.pipe(optimizejs)
	.pipe(dest, paths.scripts.output)
	.pipe(rename, {suffix: '.min'})
	.pipe(uglify)
	.pipe(optimizejs)
	.pipe(header, banner.min, {package: package})
	.pipe(dest, paths.scripts.output);

// Lint, minify, and concatenate scripts
var buildScripts = function (done) {

	// Make sure this feature is activated before running
	if (!settings.scripts) return done();

	// Run tasks on script files
	src(paths.scripts.input)
		.pipe(plumber())
		.pipe(flatmap(function(stream, file) {

			// If the file is a directory
			if (file.isDirectory()) {

				// Setup a suffix variable
				var suffix = '';

				// If separate polyfill files enabled
				if (settings.polyfills) {

					// Update the suffix
					suffix = '.polyfills';

					// Grab files that aren't polyfills, concatenate them, and process them
					src([file.path + '/*.js', '!' + file.path + '/*' + paths.scripts.polyfills])
						.pipe(concat(file.relative + '.js'))
						.pipe(jsTasks());

				}

				// Grab all files and concatenate them
				// If separate polyfills enabled, this will have .polyfills in the filename
				src(file.path + '/*.js')
					.pipe(concat(file.relative + suffix + '.js'))
					.pipe(jsTasks());

				return stream;

			}

			// Otherwise, process the file
			return stream.pipe(jsTasks());

		}));

	// Signal completion
	done();

};

// Lint scripts
var lintScripts = function (done) {

	// Make sure this feature is activated before running
	if (!settings.scripts) return done();

	// Lint scripts
	src(paths.scripts.input)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));

	// Signal completion
	done();

};

// Process, lint, and minify Sass files
var buildStyles = function (done) {

	// Make sure this feature is activated before running
	if (!settings.styles) return done();

	// Run tasks on all Sass files
	src(paths.styles.input)
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'expanded',
			sourceComments: true
		}))
		.pipe(prefix({
			browsers: ['last 2 version', '> 0.25%'],
			cascade: true,
			remove: true
		}))
		.pipe(header(banner.full, { package : package }))
		.pipe(dest(paths.styles.output))
		.pipe(rename({suffix: '.min'}))
		.pipe(minify({
			discardComments: {
				removeAll: true
			}
		}))
		.pipe(header(banner.min, { package : package }))
		.pipe(dest(paths.styles.output));

	// Signal completion
	done();

};

// Process css files
var buildPluginStyles = function (done) {

	// Make sure this feature is activated before running
	if (!settings.pluginStyles) return done();

	// Run tasks on all CSS files
	src(paths.pluginStyles.input)
		.pipe(cleanCSS({
				debug: true,
				compatibility: 'ie8',
				level: {
						1: {
								specialComments: 0,
						},
				},
		}))
		.pipe(concat('plugin-styles.min.css'))
		.pipe(dest(paths.styles.output))

	// Signal completion
	done();

};


// Optimize SVG files
var buildSVGs = function (done) {

	// Make sure this feature is activated before running
	if (!settings.svgs) return done();

	// Optimize SVG files
	src(paths.svgs.input)
		.pipe(plumber())
		.pipe(svgmin())
		.pipe(dest(paths.svgs.output));

	// Signal completion
	done();

};

// Copy wordpress files into output folder
var copyWordpressCore = function (done) {

	// Make sure this feature is activated before running
	if (!settings.wordpressCore) return done();

	// Copy static files
	src(paths.wordpressCore.input)
		.pipe(dest(paths.wordpressCore.output));

	// Signal completion
	done();

};

// Copy wordpress files into output folder
var copyWordpressSetupFiles = function (done) {

	// Make sure this feature is activated before running
	if (!settings.wordpressSetup) return done();

	// Copy static files
	src(paths.wordpressSetup.input)
		.pipe(dest(paths.wordpressSetup.output));

	// Signal completion
	done();

};

// Copy theme plugin files into output folder
var copyWordpressPlugins = function (done) {

	// Make sure this feature is activated before running
	if (!settings.wordpressPlugins) return done();

	// Copy static files
	src(paths.wordpressPlugins.input)
		.pipe(dest(paths.wordpressPlugins.output));

	// Signal completion
	done();

};

// Copy static files into output folder
var copyLibraryConditionizr = function (done) {

	// Install script files for counterup
	if (!libraries.conditionizr.enabled) return done();

		// Copy static files
		src(libraries.conditionizr.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

// Copy static files into output folder
var copyLibraryResetCss = function (done) {

	// Install script files for counterup
	if (!libraries.resetCss.enabled) return done();

		// Copy static files
		src(libraries.resetCss.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

// Copy static files into output folder
var copyLibraryAnimateCss = function (done) {

	// Install script files for counterup
	if (!libraries.animateCss.enabled) return done();

		// Copy static files
		src(libraries.animateCss.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

// Copy static files into output folder
var copyLibraryCounterup = function (done) {

	// Install script files for counterup
	if (!libraries.counterup.enabled) return done();

		// Copy static files
		src(libraries.counterup.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

var copyLibrarySlick_carousel = function (done) {

	// Install script files for slick_carousel
	if (!libraries.slick_carousel.enabled) return done();

		// Copy static files
		src(libraries.slick_carousel.js)
			.pipe(dest(libraries.installJs));

		src(libraries.slick_carousel.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

var copyLibraryIsotope_layout = function (done) {

	// Install script files for isotope_layout
	if (!libraries.isotope_layout.enabled) return done();

		// Copy static files
		src(libraries.isotope_layout.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

var copyLibraryFlickity = function (done) {

	// Install script files for flickity
	if (!libraries.flickity.enabled) return done();

		// Copy static files
		src(libraries.flickity.js)
			.pipe(dest(libraries.installJs));

		src(libraries.flickity.css)
			.pipe(dest(libraries.installCss));
	// Signal completion
	done();
};

var copyLibraryFeatherlight = function (done) {

	// Install script files for featherlight
	if (!libraries.featherlight.enabled) return done();

		// Copy static files
		src(libraries.featherlight.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

var copyLibraryFontAwesome = function (done) {

	// Install script files for fontAwesome
	if (!libraries.fontAwesome.enabled) return done();

		// Copy static files
		src(libraries.fontAwesome.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

// Copy static files into output folder
var copyLibraryHamburgers = function (done) {

	// Install script files for counterup
	if (!libraries.hamburgers.enabled) return done();

		// Copy static files
		src(libraries.hamburgers.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

var copyLibraryMatchheight = function (done) {

	// Install script files for matchheight
	if (!libraries.matchheight.enabled) return done();

		// Copy static files
		src(libraries.matchheight.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

var copyLibrarySelect2 = function (done) {

	// Install script files for select2
	if (!libraries.select2.enabled) return done();

		// Copy static files
		src(libraries.select2.js)
			.pipe(dest(libraries.installJs));
		
		src(libraries.select2.css)
			.pipe(dest(libraries.installCss));

	// Signal completion
	done();
};

var copyLibraryWaitforimages = function (done) {

	// Install script files for waitforimages
	if (!libraries.waitforimages.enabled) return done();

		// Copy static files
		src(libraries.waitforimages.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

var copyLibraryWaypoints = function (done) {

	// Install script files for waypoints
	if (!libraries.waypoints.enabled) return done();

		// Copy static files
		src(libraries.waypoints.js)
			.pipe(dest(libraries.installJs));

	// Signal completion
	done();
};

// Copy static files into output folder
var copyFiles = function (done) {

	// Make sure this feature is activated before running
	if (!settings.copy) return done();

	// Copy static files
	src(paths.copy.input)
		.pipe(dest(paths.copy.output));

	// Signal completion
	done();

};

// Reload the browser when files change
var reloadBrowser = function (done) {
	if (!settings.reload) return done();
	browserSync.reload();
	done();
};

// Watch for changes
var watchSource = function (done) {
	watch(paths.input, series(exports.default));
	done();
};


/**
 * Default Tasks
 * List of default tasks needed to reload the theme after changes
 */

 exports.default = series(
	cleanTheme,
	parallel(
		buildScripts,
		lintScripts,
		buildStyles,
		buildPluginStyles,
		buildSVGs,
		copyFiles
	)
);

/**
 * Wordpress Setup
 * Install wordpress and any additional files
 */

exports.wordpress = series(
	parallel(
		copyWordpressCore,
		copyWordpressSetupFiles,
		copyWordpressPlugins,
		copyFiles
	)
);

/**
 * Install js libraries
 * List of libraries to be installed
 */

exports.jsLibrarys = series(
	parallel(
		copyLibraryResetCss,
		copyLibraryConditionizr,
		copyLibraryAnimateCss,
		copyLibraryCounterup,
		copyLibrarySlick_carousel,
		copyLibraryIsotope_layout,
		copyLibraryFlickity,
		copyLibraryFeatherlight,
		copyLibraryFontAwesome,
		copyLibraryHamburgers,
		copyLibraryMatchheight,
		copyLibrarySelect2,
		copyLibraryWaitforimages,
		copyLibraryWaypoints
	)
);

/**
 * Theme Setup
 * List of tasts to setup the theme
 * Copy scripts to /src
 * Install Wordpress and wordpress plugins
 * Should leave you with default wordpress website installed
 */

 exports.theme = series(
	exports.jsLibrarys,
	exports.wordpress
);

/**
 * Watch and reload theme files
 * Watch files for changes and reload /dist
 * Clean theme folder
 * Reload js, css, svgs
 * Reload theme files
 */

exports.watch = series(
	exports.default,
	watchSource
);

/**
 * Clean /dist folder
 * Remove all files and folders
 */

exports.clean = series(
	cleanDist
);