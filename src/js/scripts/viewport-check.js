$(document).ready(function(){

	var zoomIn = $('.viewport_check_zoomIn:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport zoomIn animated');

		this.destroy();
	}, {
		offset: '90%'
	});

	var bounceIn = $('.viewport_check_bounceIn:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport bounceIn animated');

		this.destroy();
	}, {
		offset: '90%'
	});

	var FadeInRight = $('.viewport_check_fadeInRight:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport slideInRight animated');

		this.destroy();
	}, {
		offset: '90%'
	});

	var SlideInUp = $('.viewport_check_slideInUp:not(.in_viewport)').waypoint(function () {
		$(this.element).addClass('in_viewport slideInUp animated');

		this.destroy();
	}, {
		offset: '90%'
	});
});