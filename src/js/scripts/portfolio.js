$(document).ready(function(){


	// init Isotope
	var $grid = $('.grid-portfolio').isotope({
		itemSelector: '.element-item',
		layoutMode: 'fitRows'
	});

	// bind filter button click
	$('.filters-button-group').on( 'click', 'button', function() {
		var filterValue = $(this).attr('data-filter');
		// use filterFn if matches value
		filterValue = filterValue || filterValue;
		$grid.isotope({ filter: filterValue });
	});

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );
		$buttonGroup.on( 'click', 'button', function() {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});

	// Change tile on hover
	$('.isotope-wrap .element-item').hover(function() {
		/* Mouse enter */
		var element = $(this);
		element.addClass('hover-state');
		element.children().children('.element-content-bg').addClass('fadeIn faster animated');
		element.children().children('.element-content').addClass('bounceIn animated');

	}, function() {
		/* Mouse leave */
		var element = $(this);
		element.removeClass('hover-state');
		element.children().children('.element-content-bg').removeClass('fadeIn faster animated');
		element.children().children('.element-content').removeClass('bounceIn animated');
	});


	/*------------------------------------*\
    		PLAY MEDIA OVERLAY
	\*------------------------------------*/

	//media change on hover
	$('.portolio-audio-item-wrap .portolio-image').hover(function() {
		/* Mouse enter */
		var element = $(this);
		element.parent('.portolio-audio-item-wrap').addClass('hover-state');
		element.children('.media-play-overlay').children('.button-wrap').addClass('bounceIn animated');

	}, function() {
		/* Mouse leave */
		var element = $(this);

		if ($(".portolio-audio-item-wrap .media-playing")[1]){
			element.parent('.portolio-audio-item-wrap').removeClass('hover-state');
			element.children('.media-play-overlay').children('.button-wrap').removeClass('bounceIn animated');
		}
	});


	// Play media on click
	$('.portolio-audio-item-wrap').find('.button-wrap').on( 'click', function() {
		$(this).parent('.media-play-overlay').parent('.portolio-image').parent('.portolio-audio-item-wrap').children('.mejs-container').children('.mejs-inner').children('.mejs-controls').children('.mejs-button').click();
		$(this).parent('.media-play-overlay').parent('.portolio-image').parent('.portolio-audio-item-wrap').toggleClass('media-playing');
	});

	// $('.portolio-audio-item-wrap .audio-caption .tile-wrap').on( 'click', function() {
	// 	$(this).parent('.audio-caption').parent('.portolio-audio-item-wrap').children('.portolio-image').parent('.portolio-audio-item-wrap').children('.mejs-container').children('.mejs-inner').children('.mejs-controls').children('.mejs-button').click();
	// 	$(this).parent('.audio-caption').parent('.portolio-audio-item-wrap').children('.portolio-image').parent('.portolio-audio-item-wrap').toggleClass('media-playing');
	// 	$(this).parent('.audio-caption').parent('.portolio-audio-item-wrap').children('.portolio-image').children('.media-play-overlay').children('.button-wrap').addClass('bounceIn animated');
	// });

});