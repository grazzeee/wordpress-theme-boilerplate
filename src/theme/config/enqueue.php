<?php

    // Load header scripts
    function header_scripts() {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

            wp_deregister_script('jquery');

            wp_register_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, '1.11.3');
            wp_enqueue_script('jquery');

            wp_register_script('libraries', get_template_directory_uri() . '/assets/js/lib.min.js', array(), filemtime(get_template_directory() . '/assets/js/lib.min.js'), false);
            wp_enqueue_script('libraries');

            wp_register_script('loadscripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array(), filemtime(get_template_directory() . '/assets/js/scripts.min.js'), false);
            wp_enqueue_script('loadscripts');
        }
    }

    add_action('init', 'header_scripts');

    // Load styles
    function styles() {

        wp_enqueue_style('pluginStyles', get_template_directory_uri() . '/assets/css/style.min.css', array(), filemtime(get_template_directory() . '/assets/css/plugin-styles.min.css'), false);
        wp_enqueue_style('pluginStyles');

        wp_enqueue_style('styles', get_template_directory_uri() . '/assets/css/style.min.css', array(), filemtime(get_template_directory() . '/assets/css/style.min.css'), false);
        wp_enqueue_style('styles');

        wp_enqueue_style('googleFont', 'https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:400,500,600|Noto+Sans:400,700|Open+Sans:400,600,700|PT+Sans:400,700|Prompt:400,500,700|Raleway:400,500,600,700|Roboto|Slabo+27px|Source+Sans+Pro:400,700', false );
        wp_enqueue_style('googleFont');

    }
    add_action('wp_enqueue_scripts', 'styles');

    // ADMIN SCRIPTS
    // add_action( 'admin_enqueue_scripts', function() {
    //     wp_enqueue_script( 'handle', get_template_directory_uri() . '/assets/js-admin/admin-scripts.js', array(), filemtime(get_template_directory() . '/assets/js-admin/admin-scripts.js'), false);
    // });

    // ADMIN STYLES
    // function wpdocs_enqueue_custom_admin_style() {
    //         wp_enqueue_style('custom_wp_admin_css', get_template_directory_uri() . '/assets/css-admin/admin-style.css', array(), filemtime(get_template_directory() . '/assets/css-admin/admin-style.css'), false);
    //         wp_enqueue_style( 'custom_wp_admin_css' );
    // }
    // add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );
?>